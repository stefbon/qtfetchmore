/*
  2010, 2011, 2012, 2013 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <err.h>
#include <sys/time.h>
#include <assert.h>
#include <syslog.h>
#include <time.h>
#include <pthread.h>

#define DEFAULT_NOTIFYFS_SOCKET "/run/notifyfs.sock"
//#define LOGGING		1

#include <errno.h>

#include "connectserver-helper.h"
#include "logging.h"

struct ioqueue_struct {
    struct ioelement_struct *first;
    struct ioelement_struct *last;
    unsigned char status;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
};

static char recvbuffer[NOTIFYFS_RECVBUFFERSIZE];
static int socket_fd=-1;
static int epoll_fd=-1;
static struct epoll_eventloop_struct eventloop=EPOLL_EVENTLOOP_INIT;
static struct notifyfs_connection_struct localsocket;
static char eventloopisup=0;

static unsigned int watchid=1;
static pthread_t viewthread_id;
static unsigned char viewthread_started=0;
static struct view_struct *view=NULL;
static unsigned char statusview=0;

static int queue_index=0;
static struct ioqueue_struct notifyfs_ioqueue;

struct cacherow_struct cachedirectory[NAMEINDEX_ROOT1];
int cachetotalcount=0;
static unsigned char cacheinitialized=0;
static pthread_mutex_t cachemutex=PTHREAD_MUTEX_INITIALIZER;

int compare_entries(struct notifyfs_entry_struct *entryA, struct notifyfs_entry_struct *entryB)
{
    int res=0;

    if (entryA->nameindex_value < entryB->nameindex_value) {

	res=-1;

    } else if (entryA->nameindex_value < entryB->nameindex_value) {

	res=1;

    } else {
	char *nameA=get_data(entryA->name);
	char *nameB=get_data(entryB->name);

	res=strcmp(nameA, nameB);

    }

    return res;

}

static int determine_row(unsigned char firstletter)
{
    int i;
    int row=0;

    for (i=0;i<firstletter;i++) row+=cachedirectory[i].count;

    return row;

}

struct notifyfs_entry_struct *get_cached_entry(int row)
{
    int tmp_total=0, i=0;
    struct notifyfs_entry_struct *entry=NULL;

    pthread_mutex_lock(&cachemutex);

    while (i<NAMEINDEX_ROOT1-1) {

	if (row <  tmp_total + cachedirectory[i].count) break;
	tmp_total+=cachedirectory[i].count;
	i++;

    }

    if (i<NAMEINDEX_ROOT1-1) {
	struct cacheentry_struct *cacheentry=NULL;

	cacheentry=cachedirectory[i].cacheentry;

	while(cacheentry) {

	    if (cacheentry->ctr + tmp_total == row) break;
	    cacheentry=cacheentry->next;

	}

	if (cacheentry) entry=get_entry(cacheentry->entryindex);

    }

    pthread_mutex_unlock(&cachemutex);

    return entry;

}

static struct cacheentry_struct *lookup_next_cacheentry(int i)
{

    logoutput("lookup_next_cacheentry: i=%i", i);

    while (i<NAMEINDEX_ROOT1) {

	if (cachedirectory[i].cacheentry) break;
	i++;

    }

    if (i<NAMEINDEX_ROOT1) {

	logoutput("lookup_next_cacheentry: cacheentry found for %i", i);

	return cachedirectory[i].cacheentry;

    } else {

	logoutput("lookup_next_cacheentry: cacheentry not found");

    }

    return NULL;
}


struct cacheentry_struct *get_next_cacheentry(struct cacheentry_struct *cacheentry)
{

    if (!cacheentry) {

	cacheentry=lookup_next_cacheentry(0);

    } else {
	struct notifyfs_entry_struct *entry=get_entry(cacheentry->entryindex);

	cacheentry=cacheentry->next;

	if (! cacheentry) {
	    char *name=get_data(entry->name);
	    unsigned char firstletter=*(name) - 32;

	    cacheentry=lookup_next_cacheentry(firstletter+1);

	}

    }

    return cacheentry;

}

static unsigned char update_cachedirectory_add(int entryindex, int *row)
{
    int nreturn=0;
    struct notifyfs_entry_struct *entry=get_entry(entryindex);

    logoutput("update_cachedirectory_add");

    if (entry) {
	char *name=get_data(entry->name);
	unsigned char firstletter=*(name) - 32;
	struct cacherow_struct *cacherow=&cachedirectory[firstletter];

	*row=determine_row(firstletter);

	pthread_mutex_lock(&cachemutex);

	if (cacherow->count==0) {
	    struct cacheentry_struct *cacheentry=NULL;

	    cacheentry=malloc(sizeof(struct cacheentry_struct));

	    if (cacheentry) {

		cacherow->cacheentry=cacheentry;
		cacherow->count=1;

		cacheentry->entryindex=entryindex;
		cacheentry->ctr=0;
		cacheentry->next=NULL;
		cacheentry->prev=NULL;

		nreturn=1;
		cachetotalcount++;

	    }

	} else {
	    struct cacheentry_struct *cacheentry=NULL;
	    struct notifyfs_entry_struct *nextentry=NULL;
	    int res=0;

	    cacheentry=cacherow->cacheentry;

	    while(cacheentry) {

		nextentry=get_entry(cacheentry->entryindex);
		res=compare_entries(entry, nextentry);

		if (res<0) {
		    struct cacheentry_struct *cacheentry_new=NULL;

		    cacheentry_new=malloc(sizeof(struct cacheentry_struct));

		    if (cacheentry_new) {

			cacheentry_new->entryindex=entryindex;
			cacheentry_new->ctr=cacheentry->ctr;
			cacheentry_new->next=NULL;
			cacheentry_new->prev=NULL;

			/* next entry is bigger: insert here */

			if (cacheentry->prev) {

			    cacheentry->prev->next=cacheentry_new;
			    cacheentry_new->next=cacheentry;
			    cacheentry_new->prev=cacheentry->prev;
			    cacheentry->prev=cacheentry_new;

			} else {

			    cacheentry->prev=cacheentry_new;
			    cacheentry_new->next=cacheentry;

			    cacherow->cacheentry=cacheentry_new;

			}

			/* update the count for this row */

			while(cacheentry) {

			    cacheentry->ctr++;
			    cacheentry=cacheentry->next;

			}

			cacherow->count++;
			*row+=cacheentry_new->ctr;
			nreturn=1;
			cachetotalcount++;

		    }

		    break;

		} else if (res==0) {

		    /* the same: no update */

		    break;

		} else if (res>0) {

		    /* new entry is bigger: shift */

		    if (! cacheentry->next) {
			struct cacheentry_struct *cacheentry_new=NULL;

			cacheentry_new=malloc(sizeof(struct cacheentry_struct));

			if (cacheentry_new) {

			    cacheentry_new->entryindex=entryindex;
			    cacheentry_new->ctr=cacheentry->ctr+1;
			    cacheentry->next=cacheentry_new;
			    cacheentry_new->prev=cacheentry;
			    cacheentry_new->next=NULL;

			    cacherow->count++;
			    *row+=cacheentry_new->ctr;
			    nreturn=1;
			    cachetotalcount++;

			}

			break;

		    }

		}

		cacheentry=cacheentry->next;

	    }

	}

	pthread_mutex_unlock(&cachemutex);

    }

    return nreturn;

}

static unsigned char update_cachedirectory_remove(int entryindex, int *row)
{
    int nreturn=0;
    struct notifyfs_entry_struct *entry=get_entry(entryindex);

    logoutput("update_cachedirectory_remove");

    if (entry) {
	char *name=get_data(entry->name);
	unsigned char firstletter=*(name) - 32;
	struct cacherow_struct *cacherow=&cachedirectory[firstletter];
	struct cacheentry_struct *cacheentry=cacherow->cacheentry;

	pthread_mutex_lock(&cachemutex);

	*row=determine_row(firstletter);

	while(cacheentry) {

	    if (cacheentry->entryindex==entryindex) {

		/* found */

		if (cacheentry==cacherow->cacheentry) cacherow->cacheentry=cacheentry->next;
		if (cacheentry->prev) cacheentry->prev->next=cacheentry->next;
		if (cacheentry->next) cacheentry->next->prev=cacheentry->prev;

		cacherow->count--;
		*row+=cacheentry->ctr;
		nreturn=1;
		cachetotalcount--;

		if (cacheentry->next) {
		    struct cacheentry_struct *cacheentry_next=NULL;

		    cacheentry_next=cacheentry->next;
		    free(cacheentry);

		    while(cacheentry_next) {

			cacheentry_next->ctr--;
			cacheentry_next=cacheentry_next->next;

		    }

		} else {

		    free(cacheentry);

		}

		break;

	    }

	    cacheentry=cacheentry->next;

	}

	pthread_mutex_unlock(&cachemutex);

    }

    return nreturn;

}

unsigned char update_cachedirectory(int entryindex, int *row, int what)
{
    if (what==1) {

	return update_cachedirectory_add(entryindex, row);

    } else if (what==-1) {

	return update_cachedirectory_remove(entryindex, row);

    }

    return 0;

}

void clean_cachedirectory()
{

    if (cacheinitialized==0) {
	int i;

	for (i=0;i<NAMEINDEX_ROOT1;i++) {

	    cachedirectory[i].cacheentry=NULL;
	    cachedirectory[i].count=0;

	}

	cacheinitialized=1;

    } else {
	struct cacheentry_struct *cacheentry=NULL;
	int i;

	for(i=0;i<NAMEINDEX_ROOT1;i++) {

	    cacheentry=cachedirectory[i].cacheentry;

	    while(cacheentry) {

		cachedirectory[i].cacheentry=cacheentry->next;
		free(cacheentry);
		cacheentry=cachedirectory[i].cacheentry;

	    }

	    cachedirectory[i].count=0;

	}

    }

    cachetotalcount=0;

}

void lockview()
{
    if (view) pthread_mutex_lock(&view->mutex);
}

void unlockview()
{
    if (view) pthread_mutex_unlock(&view->mutex);
}

void waitforeventonview()
{
    if (view) pthread_cond_wait(&view->cond, &view->mutex);
}

void signalview()
{
    if (view) pthread_cond_broadcast(&view->cond);
}

/*

    get the next event from view event queue 
    for now only monitor added and removed entries (no changes...)

*/

struct ioelement_struct *get_next_ioelement(int *error)
{
    struct ioelement_struct *ioelement=NULL;

    *error=0;

    logoutput("get_next_ioelement: local index %i, view queue index %i", queue_index, view->queue_index);

    if (queue_index != view->queue_index) {
	struct fseventmask_struct *fseventmask;

	fseventmask=&view->eventqueue[queue_index].fseventmask;

	ioelement=malloc(sizeof(struct ioelement_struct));

	if (ioelement) {

	    ioelement->entry=view->eventqueue[queue_index].entry;
	    replace_fseventmask(&ioelement->fseventmask, fseventmask);
	    ioelement->next=NULL;
	    ioelement->prev=NULL;

	    queue_index=(queue_index+1) % NOTIFYFS_VIEWQUEUE_LEN;

	} else {

	    *error=ENOMEM;

	}

    }

    return ioelement;

}

/* put an ioelement to io queue */

void put_ioelement(struct ioelement_struct *ioelement)
{

    pthread_mutex_lock(&notifyfs_ioqueue.mutex);

    if (notifyfs_ioqueue.first) notifyfs_ioqueue.first->prev=ioelement;
    ioelement->next=notifyfs_ioqueue.first;
    notifyfs_ioqueue.first=ioelement;

    if (! notifyfs_ioqueue.last) notifyfs_ioqueue.last=ioelement;

    pthread_cond_broadcast(&notifyfs_ioqueue.cond);

    pthread_mutex_unlock(&notifyfs_ioqueue.mutex);

}

/* remove an ioelement from the ioqueue */

void get_ioelement(struct ioelement_struct *ioelement)
{

    // pthread_mutex_lock(&notifyfs_ioqueue.mutex);

    if (ioelement->next) ioelement->next->prev=ioelement->prev;
    if (ioelement->prev) ioelement->prev->next=ioelement->next;

    if (notifyfs_ioqueue.first==ioelement) notifyfs_ioqueue.first=ioelement->next;
    if (notifyfs_ioqueue.last==ioelement) notifyfs_ioqueue.last=ioelement->prev;

    // pthread_mutex_unlock(&notifyfs_ioqueue.mutex);

}


void process_viewio(void *data)
{
    int error=0;
    struct ioelement_struct *ioelement=NULL;

    viewthread_started=1;

    logoutput("process_viewio");

    while(1) {

	/* wait for event */

	lockview();

	statusview=view->status;

	waitforeventonview();

	while(1) {

	    /* check why there has been a signal */

	    if (statusview!=view->status) {

		pthread_mutex_lock(&notifyfs_ioqueue.mutex);
		pthread_cond_broadcast(&notifyfs_ioqueue.cond);
		pthread_mutex_unlock(&notifyfs_ioqueue.mutex);

		statusview=view->status;

	    }

	    /* get an io element from view */

	    error=0;
	    ioelement=get_next_ioelement(&error);

	    if (ioelement) {

		put_ioelement(ioelement);

	    } else if (error!=0) {

		/* no error: queue is empty */

		logoutput("process_viewio: no ioelement, error %i", error);

	    } else {

		break;

	    }

	}

	unlockview();

    }

}

int ChangeView(int entry)
{
    int res=0;
    struct timespec rightnow;

    if (! view) {

	view=notifyfs_malloc_view();

	if (! view) {

	    res=-ENOMEM;
	    goto out;

	}

	view->pid=getpid();

    }

    if (viewthread_started==0) {

	res=pthread_create(&viewthread_id, NULL, (void *) &process_viewio, NULL);

	if ( res!=0 ) {

	    logoutput("ChangeView: pthread_create returned %i", res);

	}

    }

    get_current_time(&rightnow);

    view->parent_entry=entry;
    view->status=0;
    view->change_time.tv_sec=rightnow.tv_sec;
    view->change_time.tv_nsec=rightnow.tv_nsec;

    view->mode=NOTIFYFS_CLIENTMODE_HIDEDOTNAMES;

    out:

    return (res<0) ? res : view->index;

}

struct view_struct *getView()
{
    return view;
}

int getStatusView()
{
    if (view) return view->status;

    return 0;
}

void setStatusView(unsigned char status)
{
    if (view) view->status=status;
}

int getViewCount()
{
    if (view) return view->count;
    return 0;
}

void lock_ioqueue()
{
    pthread_mutex_lock(&notifyfs_ioqueue.mutex);
}

void unlock_ioqueue()
{
    pthread_mutex_unlock(&notifyfs_ioqueue.mutex);
}

/* get the (oldest) ioelement from queue */

struct ioelement_struct *get_ioelement_from_queue()
{

    return notifyfs_ioqueue.last;

}

/* wait for signal on io queue */

void waitforioelement()
{

    int res=pthread_cond_wait(&notifyfs_ioqueue.cond, &notifyfs_ioqueue.mutex);

}

void init_notifyfs_ioqueue()
{

    notifyfs_ioqueue.first=NULL;
    notifyfs_ioqueue.last=NULL;

    notifyfs_ioqueue.status=0;

    pthread_mutex_init(&notifyfs_ioqueue.mutex, NULL);
    pthread_cond_init(&notifyfs_ioqueue.cond, NULL);

}

unsigned char getIOqueueStatus()
{
    return notifyfs_ioqueue.status;
}

void setIOqueueStatus(unsigned char status)
{
    notifyfs_ioqueue.status=status;
}

unsigned char fsevent_is_add(struct fseventmask_struct *fseventmask)
{
    unsigned char isadded=0;

    if ((fseventmask->cache_event & NOTIFYFS_FSEVENT_CACHE_ADDED) || (fseventmask->move_event & (NOTIFYFS_FSEVENT_MOVE_CREATED | NOTIFYFS_FSEVENT_MOVE_MOVED_TO))) isadded=1;

    return isadded;

}

unsigned char fsevent_is_del(struct fseventmask_struct *fseventmask)
{
    unsigned char isdel=0;

    if ((fseventmask->cache_event & NOTIFYFS_FSEVENT_CACHE_REMOVED) || (fseventmask->move_event & (NOTIFYFS_FSEVENT_MOVE_DELETED | NOTIFYFS_FSEVENT_MOVE_MOVED_FROM))) isdel=1;

    return isdel;

}

//	if ((fseventmask->cache_event & NOTIFYFS_FSEVENT_CACHE_ADDED) || (fseventmask->move_event & (NOTIFYFS_FSEVENT_MOVE_CREATED | NOTIFYFS_FSEVENT_MOVE_MOVED_TO))) {

	    /* entry is new */

//	    *what=1;
//	    entry=view->eventqueue[queue_index].entry;

//	} else if ((fseventmask->cache_event & NOTIFYFS_FSEVENT_CACHE_REMOVED) || (fseventmask->move_event & (NOTIFYFS_FSEVENT_MOVE_DELETED | NOTIFYFS_FSEVENT_MOVE_MOVED_FROM))) {

	    /* entry is removed */

//	    *what=-1;
//	    entry=view->eventqueue[queue_index].entry;

//	}





/* send a client message, from client to server, like:
   - register a client as app or as fs or both
   - signoff as client at server
   - give messagemask, to inform the server about what messages to receive, like mountinfo
   */

void send_register_to_server()
{
    if (socket_fd>0) {
	uint64_t unique=new_uniquectr();

	send_register_message(socket_fd, unique, getpid(), NOTIFYFS_CLIENTTYPE_APP, NULL, 0);

    }

}

void handle_reply_message(int fd, void *data, struct notifyfs_reply_message *reply_message, void *buff, int len, unsigned char remote)
{

    if (reply_message->error>0) {

	logoutput("handle_reply_message: got reply with error (%i:%s)", reply_message->error, strerror(reply_message->error));

    }

    process_notifyfs_reply(reply_message->unique, reply_message->error);

}

void handle_fsevent_message(int fd, void *data, struct notifyfs_fsevent_message *fsevent_message, void *buff, int len, unsigned char remote)
{
    struct notifyfs_entry_struct *entry=NULL;

    /* just log for now */

    logoutput("handle_fsevent_message: event on watch id %i, detect time %li.%li", fsevent_message->watch_id, fsevent_message->detect_time.tv_sec,fsevent_message->detect_time.tv_nsec);

    entry=get_entry(fsevent_message->entry);

    if (entry) {
        char *name=NULL;
	struct fseventmask_struct *fseventmask=&(fsevent_message->fseventmask);

	name=get_data(entry->name);

	logoutput("handle_fsevent_message: entry %s, %i:%i:%i:%i", name, fseventmask->attrib_event, fseventmask->xattr_event, fseventmask->file_event, fseventmask->move_event);

    }

}

int process_server_event(struct notifyfs_connection_struct *connection, uint32_t events)
{

    if (events & ( EPOLLHUP | EPOLLRDHUP ) ) {

	logoutput("process_server_event: hangup of remote site");

    } else if (events & EPOLLIN) {

	int res=receive_message(connection->fd, connection->data, events, NOTIFYFS_OWNERTYPE_SERVER, recvbuffer, NOTIFYFS_RECVBUFFERSIZE);

    }

    return 0;

}


int notifyfs_server_socket()
{
    int fd=-1;

    localsocket.fd=0;
    localsocket.data=NULL;
    localsocket.allocated=0;
    localsocket.process_event=NULL;

    init_xdata(&localsocket.xdata_socket);

    fd=create_local_clientsocket(DEFAULT_NOTIFYFS_SOCKET, &localsocket, &eventloop, process_server_event);

    logoutput("notifyfs_server_socket: connected to %i", fd);

    return fd;

}

void connectionisup(struct epoll_eventloop_struct *eventloop)
{
    eventloopisup=1;

}

void setupconnectionserver()
{
    int res;

    assign_notifyfs_message_cb_reply(handle_reply_message);
    assign_notifyfs_message_cb_fsevent(handle_fsevent_message);

    res=init_eventloop(&eventloop, 0, 0);

    logoutput("setupconnectionserver: res %i", res);

    if (res==0) {
	int fd;

	fd=notifyfs_server_socket(&eventloop, &localsocket);

	if (fd>0) {

	    socket_fd=fd;

	    start_epoll_eventloop(&eventloop, connectionisup);
	    destroy_eventloop(&eventloop);

	}

    }

}

void send_register_when_connected()
{
    int res=0;

    if (eventloopisup==1) {

	res=1;

    } else {

	res=eventloop_isup(&eventloop);

	if (res<0) {

	    logoutput("send_register_when_connected: error %i", res);

	}

    }

    if (res==1) send_register_to_server();

}

void send_setwatch(int view, char *path)
{
    if (eventloopisup==1) {
	uint64_t unique=new_uniquectr();
	int attrib_event=NOTIFYFS_FSEVENT_ATTRIB_CA | NOTIFYFS_FSEVENT_ATTRIB_SELF | NOTIFYFS_FSEVENT_ATTRIB_CHILD;
	int file_event=NOTIFYFS_FSEVENT_FILE_MODIFIED | NOTIFYFS_FSEVENT_FILE_SIZE | NOTIFYFS_FSEVENT_FILE_LOCK_ADD | NOTIFYFS_FSEVENT_FILE_LOCK_CHANGE | NOTIFYFS_FSEVENT_FILE_LOCK_REMOVE | NOTIFYFS_FSEVENT_FILE_SELF | NOTIFYFS_FSEVENT_FILE_CHILD;
	int move_event=NOTIFYFS_FSEVENT_MOVE_CA | NOTIFYFS_FSEVENT_MOVE_SELF | NOTIFYFS_FSEVENT_MOVE_CHILD;

	/* no xattr for now */

	send_setwatch_message(socket_fd, unique, watchid, path, view, attrib_event, 0, file_event, move_event);

	/* no watch administration here yet 
	    so it's possible that there are watches lying around after a while ...
	*/

	watchid++;

    }

}

