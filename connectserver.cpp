/****************************************************************************
**
** Copyright (C) 2012 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

//#define LOGGING		1

#include <QApplication>
#include "connectserver.h"
#include "logging.h"

extern "C" {
    #include "notifyfs-fsevent.h"
    #include "notifyfs-io.h"
    #include "connectserver-helper.h"

extern struct cacherow_struct cachedirectory[];
extern int cachetotalcount;

}

ServerIO::ServerIO(): eventloopSetup(false) 
{
}

int ServerIO::useSharedMemoryNotifyfs()
{

    logoutput("useSharedMemoryNotifyfs");

    return connect_sharedmemory_notifyfs();

}


void ServerIO::setupConnectionServer() {

    logoutput("setupConnectionServer");

    if (eventloopSetup) return;
    eventloopSetup=true;
    setupconnectionserver();

}

bool ServerIO::connectionUP()
{
    return eventloopSetup;

}

void ServerIO::registerAtServer()
{

    logoutput("registerAtServer");

    send_register_when_connected();

}

void ServerIO::sendwatchToServer(int view, QByteArray path)
{

    logoutput("sendwatchToServer");

    if (path.isEmpty()) {

	send_setwatch(view, NULL);

    } else {

	send_setwatch(view, path.data());

    }

}

void ServerIO::synchronize_cache()
{
    struct notifyfs_entry_struct *entrySHM, *entryCache, *parent=NULL;
    struct cacheentry_struct *cacheentry=NULL, *cacheentry_keep=NULL;
    int res, i=0, inoindex, baserow=0, tmptotalcount=0;
    struct directory_struct *directory=NULL;
    struct notifyfs_inode_struct *inode=NULL;
    struct view_struct *view=getView();
    unsigned char ignoredotfiles=0;

    logoutput("synchronize_cache: (cachetotalcount=%i)", cachetotalcount);

    if (view) {

	parent=get_entry(view->parent_entry);

	if (view->mode & NOTIFYFS_CLIENTMODE_HIDEDOTNAMES) ignoredotfiles=1;

    }

    if (! parent) {

	logoutput("synchronize_cache: no parent ..");
	return;

    }

    inode=get_inode(parent->inode);
    inoindex=inode->ino % DIRECTORYINDEX_SIZE;

    directory=get_directory(inoindex);

    readlock_directory(directory);

    while (i<NAMEINDEX_ROOT1) {

	if (directory->nameindex[i]==-1) {

	    if (cachedirectory[i].count>0) {
		int row1=baserow, row2=row1+cachedirectory[i].count-1;

		cacheentry=cachedirectory[i].cacheentry;

		while(cacheentry) {

		    cachedirectory[i].cacheentry=cacheentry->next;
		    free(cacheentry);
		    cacheentry=cachedirectory[i].cacheentry;

		}

		emit entryremovedevent(row1, row2);

		cachedirectory[i].count=0;

	    }

	} else {

	    entrySHM=get_entry(directory->nameindex[i]);

	    while(entrySHM) {

		if (entrySHM->parent==parent->index) {

		    if (ignoredotfiles==1) {
			char *name=get_data(entrySHM->name);

			if (*name!='.') break;

		    } else {

			break;

		    }

		}

		entrySHM=get_entry(entrySHM->name_next);

	    }

	    cacheentry=cachedirectory[i].cacheentry;
	    cacheentry_keep=cacheentry;

	    while(1) {

		entryCache=NULL;

		if ( entrySHM && cacheentry) {

		    if (entrySHM->index == cacheentry->entryindex) {

			res=0;

		    } else {

			entryCache=get_entry(cacheentry->entryindex);
			res=compare_entries(entrySHM, entryCache);

		    }

		} else if (entrySHM && ! cacheentry) {

		    res=-1;

		} else if ( ! entrySHM && cacheentry) {

		    entryCache=get_entry(cacheentry->entryindex);
		    res=1;

		} else {

		    break;

		}

		if (res==0) {

		    /* step in shm */

		    entrySHM=get_entry(entrySHM->name_next);

		    while(entrySHM) {

			if (entrySHM->parent==parent->index) {

			    if (ignoredotfiles==1) {
				char *name=get_data(entrySHM->name);

				if (*name!='.') break;

			    } else {

				break;

			    }

			}

			entrySHM=get_entry(entrySHM->name_next);

		    }

		    /* step in cache */
		    cacheentry_keep=cacheentry;
		    cacheentry=cacheentry->next;

		    tmptotalcount++;


		} else if (res>0) {
		    char *name=get_data(entryCache->name);
		    unsigned char i=*(name) - 32;
		    int ctr=cacheentry->ctr;

		    logoutput("synchronize_cache: %s not found in shared memory", name);

		    /*
			entry in view is not found in shm: remove it from view 
			note that when deleting the row automatically the next index becomes this row
		    */

		    /*
			here a simple remove of the cacheentry...
			step in cache
		    */

		    if (cacheentry==cachedirectory[i].cacheentry) cachedirectory[i].cacheentry=cacheentry->next;
		    if (cacheentry->prev) cacheentry->prev->next=cacheentry->next;
		    if (cacheentry->next) {
			struct cacheentry_struct *cacheentry_next=cacheentry->next;

			cacheentry->next->prev=cacheentry->prev;

			free(cacheentry);
			cacheentry=cacheentry_next;

			while(cacheentry_next) {

			    cacheentry_next->ctr--;
			    cacheentry_next=cacheentry_next->next;

			}

		    } else {

			free(cacheentry);
			cacheentry=NULL;

		    }

		    cachedirectory[i].count--;

		    emit entryremovedevent(baserow+ctr, baserow+ctr);

		} else if (res<0) {
		    char *nameSHM=get_data(entrySHM->name);
		    unsigned char i=*(nameSHM) - 32;

		    logoutput("synchronize_cache: %s not found in cache", nameSHM);

		    /*
			entry in shm is not found in view: add it to view
			note that when inserting the row the next item is at row+1*/

		    /*
			here add the entry to the cache (before cacheentry )
			step in shm
		    */

		    if (cacheentry) {
			struct cacheentry_struct *cacheentry_new=NULL;

			cacheentry_new=(struct cacheentry_struct *)malloc(sizeof(struct cacheentry_struct));

			if (cacheentry_new) {

			    cacheentry_new->entryindex=entrySHM->index;
			    cacheentry_new->ctr=cacheentry->ctr;
			    cacheentry->ctr++;

			    if (cacheentry->next) {
				struct cacheentry_struct *cacheentry_next=cacheentry->next;

				while(cacheentry_next) {

				    cacheentry_next->ctr++;
				    cacheentry_next=cacheentry_next->next;

				}

			    }

			    cacheentry_new->next=NULL;
			    cacheentry_new->prev=NULL;

			    if (cacheentry->prev) {

				cacheentry->prev->next=cacheentry_new;
				cacheentry_new->next=cacheentry;
				cacheentry_new->prev=cacheentry->prev;
				cacheentry->prev=cacheentry_new;

			    } else {

				cacheentry->prev=cacheentry_new;
				cacheentry_new->next=cacheentry;

				cachedirectory[i].cacheentry=cacheentry_new;

			    }

			    cachedirectory[i].count++;

			    emit entryaddedevent(baserow+cacheentry_new->ctr, baserow+cacheentry_new->ctr);

			    cacheentry_keep=cacheentry_new;

			}

		    } else if (cacheentry_keep) {
			struct cacheentry_struct *cacheentry_new=NULL;

			cacheentry_new=(struct cacheentry_struct *)malloc(sizeof(struct cacheentry_struct));

			if (cacheentry_new) {

			    cacheentry_new->entryindex=entrySHM->index;
			    cacheentry_new->ctr=cacheentry_keep->ctr+1;

			    cacheentry_new->next=NULL;
			    cacheentry_new->prev=cacheentry_keep;
			    cacheentry_keep->next=cacheentry_new;

			    cachedirectory[i].count++;
			    cachetotalcount++;

			    emit entryaddedevent(baserow+cacheentry_new->ctr, baserow+cacheentry_new->ctr);

			    cacheentry_keep=cacheentry_new;

			}

		    } else {
			struct cacheentry_struct *cacheentry_new=NULL;

			cacheentry_new=(struct cacheentry_struct *)malloc(sizeof(struct cacheentry_struct));

			if (cacheentry_new) {

			    cacheentry_new->entryindex=entrySHM->index;
			    cacheentry_new->ctr=0;
			    cacheentry_new->next=NULL;
			    cacheentry_new->prev=NULL;

			    cachedirectory[i].count++;
			    cachetotalcount++;
			    cachedirectory[i].cacheentry=cacheentry_new;

			    emit entryaddedevent(baserow+cacheentry_new->ctr, baserow+cacheentry_new->ctr);

			    cacheentry_keep=cacheentry_new;

			}

		    }

		    /* step in shm */

		    entrySHM=get_entry(entrySHM->name_next);

		    while(entrySHM) {

			if (entrySHM->parent==parent->index) {

			    if (ignoredotfiles==1) {
				char *name=get_data(entrySHM->name);

				if (*name!='.') break;

			    } else {

				break;

			    }

			}

			entrySHM=get_entry(entrySHM->name_next);

		    }

		    tmptotalcount++;

		}

	    }

	}

	baserow+=cachedirectory[i].count;
	i++;

    }

    cachetotalcount=tmptotalcount;

    readunlock_directory(directory);

    logoutput("synchronize_cache: ready, cachetotalcount=%i", cachetotalcount);

}

void ServerIO::processIOQueue()
{
    struct ioelement_struct *ioelement=NULL;
    unsigned char status=0;

    logoutput("processIOQueue");

    init_notifyfs_ioqueue();

    while(1) {

	/*
	    get a ioelement from internal queue
	    when found process that
	    when not found wait for one

	*/


	lock_ioqueue();

	status=getStatusView();

	waitforioelement();

	unlock_ioqueue();

	while (1) {

	    /* first handle eventual status changes */

	    if (status!=getStatusView()) {

		if (getStatusView()==NOTIFYFS_VIEWSTATUS_SYNCLEVEL1) {

		    synchronize_cache();

		    status=getStatusView();

		}

	    }

	    /* next process the internal queue */

	    lock_ioqueue();

	    ioelement=get_ioelement_from_queue();

	    if (ioelement) get_ioelement(ioelement);

	    unlock_ioqueue();

	    if (ioelement) {

		/*
		    process the event: only add and remove for now
		*/

		if (fsevent_is_add(&ioelement->fseventmask)==1) {
		    int row=0;

		    if (update_cachedirectory(ioelement->entry, &row, 1)==1) {

			emit entryaddedevent(row, row);

		    }

		} else if (fsevent_is_del(&ioelement->fseventmask)==1) {
		    int row=0;

		    if (update_cachedirectory(ioelement->entry, &row, -1)==1) {

			emit entryremovedevent(row, row);

		    }

		} else {

		    logoutput("processIOQueue: ioelement for entry %i not reckognized", ioelement->entry);

		}

		free(ioelement);
		ioelement=NULL;

	    } else {

		break;

	    }

	}

    }

}
