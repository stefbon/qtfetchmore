/****************************************************************************
**
** Copyright (C) 2012 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include "connectserver.h"
#include "filelistmodel.h"

#include <QObject>
#include <QApplication>
#include <QPalette>
#include <QBrush>
#include <QThread>

extern "C" {
    #include "notifyfs-fsevent.h"
    #include "notifyfs-io.h"
    #include "logging.h"
    #include "connectserver-helper.h"

extern int cachetotalcount;

}


FileListModel::FileListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

/* slots */

void FileListModel::entryadded(int row1, int row2)
{
    struct notifyfs_entry_struct *parent=NULL;

    parent=get_entry(parentEntry);
    if (! parent) return;

    logoutput("FileListModel::entryadded");

    beginInsertRows(QModelIndex(), row1, row2);
    totalCount += (row2 - row1 + 1);
    endInsertRows();

}

void FileListModel::entryremoved(int row1, int row2)
{
    struct notifyfs_entry_struct *parent=NULL;

    parent=get_entry(parentEntry);
    if (! parent) return;

    logoutput("FileListModel::entryremoved");

    beginRemoveRows(QModelIndex(), row1, row2);
    totalCount -= (row2 - row1 + 1);
    endRemoveRows();

}

int FileListModel::rowCount(const QModelIndex & /* parent */) const
{
    /* what to do here: return the number of entries in directory parent

	this is a value which is changed dynamically

    */

    return totalCount;

}

QVariant FileListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {

	logoutput("data: no valid index");
	return QVariant();

    }

    if (role != Qt::DisplayRole) return QVariant();

    if (index.row()>=totalCount) {

	logoutput("data: index row %i bigger than total %i", index.row(), totalCount);
	return QVariant();

    }

    if (index.row()>=0) {
	struct notifyfs_entry_struct *entry=NULL;

	entry=get_cached_entry(index.row());

	if (entry) {
	    char *name=get_data(entry->name);
	    QString entryName = QString::fromAscii(name, strlen(name));

	    // logoutput("data: name %s, entryindex %i, row %i", name, entry->index, index.row());

	    return entryName;

	} else {

	    logoutput("data: no entry found for row %i, internal id %i", index.row(), index.internalId());

	}

    }

    return QVariant();

}


bool FileListModel::canFetchMore(const QModelIndex & /* index */) const
{

    logoutput("canFetchMore");

    /* check three parameters:
	- getViewCount, is what notifyfs reports 
	- getcachedtotalcount, is what is in cache
	- totalCount, is the number of rows in the view
    */

    if (totalCount<cachetotalcount) {

//	logoutput("canFetchMore: totalCount %i less then available %i", totalCount, getViewCount());
	return true;


    }

//    logoutput("canFetchMore: totalCount %i versus available %i", totalCount, getViewCount());
    return false;

}

void FileListModel::fetchMore(const QModelIndex & /* index */)
{
    int count=totalCount;

    logoutput("fetchMore");

    if (count<cachetotalcount) {
	int remainder =  cachetotalcount - count;
	int itemsToFetch = qMin(100, remainder);

	beginInsertRows(QModelIndex(), count, count+itemsToFetch-1);
	totalCount += itemsToFetch;
	endInsertRows();

    }

}

void FileListModel::setDirPath(const QString &path)
{
    int i;

    parentPath.resize(path.length());

    for (i=0;i<path.length();i++) {

	parentPath.replace(i, 1, path.mid(i, 1));

    }

}

void FileListModel::list()
{
    struct notifyfs_entry_struct *parent=NULL;
    QByteArray newPath;

    if (parentPath.isEmpty()) return;

    newPath = parentPath.toLatin1();

    if ( ! oldPath.isEmpty()) {

	/* check the path is really changed */

	if (qstrcmp(oldPath.data(), newPath.data())==0) return;

    }

    if (oldPath.size() != newPath.size()) oldPath.resize(newPath.size());
    qstrcpy(oldPath.data(), newPath.data());

    parent = check_notifyfs_path(newPath.data());

    if (parent) {

	/* entry parent does exist */

	if (parent->index != parentEntry) {
	    int viewIndex;

	    parentEntry=parent->index;
    	    viewIndex=ChangeView(parentEntry);

	    beginResetModel();
	    totalCount=0;
	    clean_cachedirectory();
	    endResetModel();

	    if (viewIndex>=0) {

		lockview();
		setStatusView(0);
		unlockview();

		send_setwatch(viewIndex, NULL);

	    }

	}

    } else {
	int viewIndex;

	viewIndex=ChangeView(-1);

	beginResetModel();
	totalCount=0;
	clean_cachedirectory();
	endResetModel();

	lockview();
	setStatusView(0);
	unlockview();


	/* directory does not exist in the cache */

	send_setwatch(viewIndex, newPath.data());

    }

}

void FileListModel::init()
{
    QThread *thread;

    entryCount=0;
    totalCount=0;

    oldPath=QByteArray();
    parentPath=QString();

    parentEntry=-1;

    /*
	start a thread to process the events on the internal queue
    */

    thread = new QThread;
    queueio = new ServerIO;

    queueio->moveToThread(thread);
    thread->start();
    QMetaObject::invokeMethod(queueio, "processIOQueue", Qt::QueuedConnection);

    connect(queueio, SIGNAL(entryaddedevent(int, int)), this, SLOT(entryadded(int, int)));
    connect(queueio, SIGNAL(entryremovedevent(int, int)), this, SLOT(entryremoved(int, int)));
    // connect(queueio, SIGNAL(viewSyncStatus3()), this, SLOT(syncView()));

}
