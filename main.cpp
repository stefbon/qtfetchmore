/****************************************************************************
**
** Copyright (C) 2012 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

//#define LOGGING		1

extern "C" {
    #include "notifyfs-fsevent.h"
    #include "notifyfs-io.h"
}

#include <QApplication>
#include <QThread>
#include "window.h"
#include "connectserver.h"
#include "logging.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    Window window;
    QThread *thread;
    ServerIO *serverio = new ServerIO;
    int res;

    res=serverio->useSharedMemoryNotifyfs();

    if (res<0) {

	logoutput("Error, unable to connect to shared memory(%i)", res);
	return 0;

    }

    thread = new QThread;

    /*
	start a thread to setup the connection to the notifyfs server
	(send an receive messages via the socket)
    */

    serverio->moveToThread(thread);
    thread->start();
    QMetaObject::invokeMethod(serverio, "setupConnectionServer", Qt::QueuedConnection);

    /* send a message to the server to register */

    serverio->registerAtServer();


#if defined(Q_OS_SYMBIAN)
    window.showMaximized();
#else
    window.show();
#endif

    return app.exec();

}
