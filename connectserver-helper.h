/*
  2010, 2011, 2012, 2013 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/


#define DEFAULT_NOTIFYFS_SOCKET "/run/notifyfs.sock"

#include "notifyfs-fsevent.h"
#include "notifyfs-io.h"
#include "message-base.h"
#include "message-receive.h"
#include "message-send.h"
#include "epoll-utils.h"
#include "socket.h"
#include "utils.h"

struct cacheentry_struct {
    int entryindex;
    int ctr;
    struct cacheentry_struct *next;
    struct cacheentry_struct *prev;
};

struct cacherow_struct {
    struct cacheentry_struct *cacheentry;
    int count;
};

struct ioelement_struct {
    int entry;
    struct fseventmask_struct fseventmask;
    struct ioelement_struct *next;
    struct ioelement_struct *prev;
};

/* prototypes */

int compare_entries(struct notifyfs_entry_struct *entryA, struct notifyfs_entry_struct *entryB);
unsigned char update_cachedirectory(int entryindex, int *row, int what);
void clean_cachedirectory();
struct notifyfs_entry_struct *get_cached_entry(int row);

/* io via filedescriptor */

void send_register_when_connected();
void send_setwatch(int view, char *path);
void setupconnectionserver();

/* io via shared memory */

struct view_struct *getView();
int ChangeView(int entry);
int getStatusView();
void setStatusView(unsigned char status);
int getViewCount();

void lockview();
void unlockview();
void waitforeventonview();
void signalview();

struct ioelement_struct *get_next_ioelement(int *error);
void put_ioelement(struct ioelement_struct *ioelement);
void get_ioelement(struct ioelement_struct *ioelement);
struct ioelement_struct *get_ioelement_from_queue();

void waitforioelement();
void lock_ioqueue();
void unlock_ioqueue();
void init_notifyfs_ioqueue();

unsigned char getIOqueueStatus();
void setIOqueueStatus(unsigned char status);

unsigned char fsevent_is_add(struct fseventmask_struct *fseventmask);
unsigned char fsevent_is_del(struct fseventmask_struct *fseventmask);


