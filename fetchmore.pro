INCLUDEPATH += ../../common/

HEADERS   = filelistmodel.h \
            window.h \
	    connectserver.h \
	    connectserver-helper.h \
	    ../../common/notifyfs-fsevent.h \
	    ../../common/notifyfs-io.h \
	    ../../common/message-base.h \
	    ../../common/message-receive.h \
	    ../../common/message-send.h \
	    ../../common/epoll-utils.h \
	    ../../common/socket.h \
	    ../../common/utils.h

SOURCES   = filelistmodel.cpp \
            main.cpp \
            window.cpp \
	    connectserver.cpp \
	    connectserver-helper.c \
	    ../../common/notifyfs-fsevent.c \
	    ../../common/notifyfs-io.c \
	    ../../common/message-receive.c \
	    ../../common/message-send.c \
	    ../../common/epoll-utils.c \
	    ../../common/socket.c \
	    ../../common/utils.c

DEFINES += "_FILE_OFFSET_BITS=64"

